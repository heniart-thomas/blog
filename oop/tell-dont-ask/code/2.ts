class IncreaseBankAccountBalance {
  constructor(private readonly _bankAccountRepository: BankAccountRepository) {}

  execute({ id, increase }: { id: string; increase: number }) {
    const account = this._bankAccountRepository.findById(id);
    account.increaseBalance(increase);
    this._bankAccountRepository.save(account);
  }
}

class BankAccount {
  constructor(
    private readonly _id: number,
    private _balance: number,
  ) {}

  increaseBalance(increase: number) {
    this._balance += increase;
  }
}

interface BankAccountRepository {
  findById(id: string): BankAccount;

  save(account: BankAccount): void;
}
