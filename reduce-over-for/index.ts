//Retrieved data from an API Response
const apiResponse = [1, 2, 3, 4];

// We cant to combine every element in the response
const calculateTotalWithoutReduce = () => {
  let total = 0;
  for (const x of apiResponse) total += x;
  return total;
};

//Not to bad but that's a lot of code for a simple sum

const calculateTotalWithReduce = () =>
  apiResponse.reduce((acc, current) => acc + current, 0);

//We can even simplify it by introducing a sum method and pass its reference to reduce
const calculateTotalWithReduce = () => apiResponse.reduce(sum, 0);
const sum = (a: number, b: number) => a + b;

// Let's image we call a currency API returning currencies rates for the last couple of days.
// Based on API documentation, the response is ordered by date ascending
export type CurrencyAPIResponse = Array<{
  date: string;
  currencies: { [currency: string]: number };
}>;
const currencyAPIiResponse = [
  {
    date: "2023-12-30",
    currencies: {
      EUR: 1,
      USD: 1.11,
    },
  },
  {
    date: "2023-12-31",
    currencies: {
      EUR: 1,
      USD: 1.1,
    },
  },
];
