class IncreaseBankAccountBalance {
  constructor(private readonly _bankAccountRepository: BankAccountRepository) {}

  execute({ id, increase }: { id: string; increase: number }) {
    const account = this._bankAccountRepository.findById(id);
    this._bankAccountRepository.save(account.withIncreasedBalance(increase));
  }
}

class BankAccount {
  constructor(
    private readonly _id: number,
    private readonly _balance: number,
  ) {}

  withIncreasedBalance(increase: number) {
    return new BankAccount(this._id, this._balance + increase);
  }
}

interface BankAccountRepository {
  findById(id: string): BankAccount;

  save(account: BankAccount): void;
}
