class CreateBankingAccount {
  constructor(
    private readonly _bankingAccountRepository: BankingAccountRepository,
    private readonly _eventPublisher: EventPublisher,
  ) {}

  execute() {
    //Some business logic and references to _eventPublisher
  }
}

interface EventPublisher {
  publish(event: Event): void;
}

class DummyEventPublisher implements EventPublisher {
  publish(event: Event): void {
    // Do nothing, we call it a dummy implementation
  }
}

// Below code is just useful for IDE completion

export type Event = {};

interface BankingAccountRepository {}

class DummyBankingAccountRepository implements BankingAccountRepository {}

const bankingAccountRepository = new DummyBankingAccountRepository();

class SpyEventPublisher implements EventPublisher {
  private readonly _events: Array<Event> = [];

  publish(event: Event): void {
    this._events.push(event);
  }

  get events() {
    return this._events;
  }
}

class CreateBankingAccount {
  constructor(
    private readonly _bankingAccountRepository: BankingAccountRepository,
    private readonly _eventPublisher: EventPublisher,
  ) {}

  execute() {
    //Some business logic
    this._eventPublisher.publish({ id: "newBankingAccountId" });
  }
}

it("publishes a banking account event", () => {
  const spy = new SpyEventPublisher();
  const useCase = new CreateBankingAccount(bankingAccountRepository, spy);

  useCase.execute();

  expect(spy.events.length).toEqual(1);
  expect(spy.events[0]).toEqual({ id: "newBankingAccountId" });
});

interface UserDataGateway {
  creditScore(email: string): number;
}

class StubUserDataGateway implements UserDataGateway {
  private _creditScoreValue: number = -1;

  creditScore(email: string): number {
    return this._creditScoreValue;
  }

  set creditScoreValue(value: number) {
    this._creditScoreValue = value;
  }
}

class CreateBankingAccount {
  constructor(
    private readonly _bankingAccountRepository: BankingAccountRepository,
    private readonly _userDataGateway: UserDataGateway,
  ) {}

  execute({ email }: { email: string }) {
    if (!this._userDataGateway.creditScore(email)) return;
    // Bank account creation logic
  }
}

it("requires a positive credit score to create a bank account", async () => {
  const stub = new StubUserDataGateway();
  const useCase = new CreateBankingAccount(
    bankingAccountRepository,
    new DummyEventPublisher(),
  );
  {
    stub.creditScoreValue = 0;
    useCase.execute({ email: "john@doe.com" });
    expect(bankingAccountRepository.accounts).toEqual([]);
  }
  {
    stub.creditScoreValue = 20;
    useCase.execute({ email: "jane@doe.com" });
    expect(bankingAccountRepository.accounts).toEqual([
      /*An account object*/
    ]);
  }
});

class StubUserDataGateway implements UserDataGateway {
  private _creditScores: { [email: string]: number } = {};

  creditScore(email: string): number {
    return this._creditScores[email] || 0;
  }

  feedWith(email, creditScore) {
    this._creditScores[email] = creditScore;
  }
}

class InMemoryBankingAccountRepository implements BankingAccountRepository {
  private readonly _accounts: Array<BankingAccount> = [];

  create(bankingAccount: BankingAccount) {
    this._accounts.push(bankingAccount);
  }

  get accounts(): Array<BankingAccount> {
    return this._accounts;
  }
}

it("creates an account", async () => {
  const repository = new InMemoryBankingAccountRepository();
  const useCase = new CreateBankingAccount(bankingAccountRepository);

  useCase.execute({ accountId: "someAccountId" });

  expect(repository.accounts).toEqual([{ accountId: "someAccountId" }]);
});

type BankingAccount = { id: string };
