Have you ever heard of the Law of Demeter? Do you know if you are violating it?

Let's dive into a real-world example: I am looking for "Test Driven Development By Example," and I know that one of my
friends (let's call him John) told me about one of his other friends (whom I don't know yet) who found a second-hand
example.
I would directly ask John to inquire where he bought it.

It would be counterproductive to ask John to meet his friend and then ask him about it.

My only interest is in John being able to find the information I am looking for.
He may even already know the answer, but I need to know.

If you are familiar with OOP, it should ring a bell and make you think about [one of the most fundamental rules of OOP:
Tell, don't ask](https://dev.to/thomasheniart/oop-fundamentals-tell-dont-ask-nh6).

---

Stay tuned for more insights! Free to follow me on this platform
and [LinkedIn](https://www.linkedin.com/in/thomas-heniart-757077120/). I share insights every week about software
design, OOP practices, and some personal project discoveries! 💻🏄
