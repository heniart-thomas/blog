// Récupération de données en ligne for example a list of brands. Each brands

// Récupération de données en lignes for example la liste des animaux -> chaque entité Animal possède une liste de country code + "001" pour dire le monde entier

// Our input supports only the standard ISO 3166 Country Codes (https://www.iso.org/iso-3166-country-codes.html)
// But the API uses a slightly different system. Instead of FR for France, it can be one to many of
/**
 *     France: FX
 *     French Guiana: GF
 *     Guadeloupe: GP
 *     Martinique: MQ
 *     Mayotte: YT
 *     Réunion: RE
 */
// And it also uses the reserved EU country code for Europe as well as 001 for the whole world

export type AnimalResponse = Array<AnimalResponseItem>;

export type AnimalResponseItem = {
  id: string;
  name: string;
  locations: Array<string>;
};

export const animalResponse: AnimalResponse = [
  {
    id: "animalId1",
    name: "Ameiva martinicensis",
    locations: ["MQ"],
  },
  {
    id: "animalId2",
    name: "Oryctolagus cuniculus",
    locations: ["EU"],
  },
  {
    id: "animalId3",
    name: "Passer domesticus",
    locations: ["001"],
  },
  {
    id: "animalId4",
    name: "Canis lupus",
    locations: ["FX", "MQ"],
  },
];

export interface AnimalAPIGateway {
  listAll(): AnimalResponse;
}

export type Country = "FR" | "UK" | "US" | "ES";

//For the purpose of the article, we assume the feature has already been implemented with a very imperative solution and has tests covering it's behaviour

export type CountryFilterStrategy = (animal: AnimalResponseItem) => boolean;

export class ListAnimal {
  constructor(
    private readonly _animalAPIGateway: AnimalAPIGateway,
    private readonly _countryFilterStrategies: Record<
      Country,
      CountryFilterStrategy
    >,
  ) {}

  execute({ country }: { country: Country }): Array<Animal> {
    const apiResponse = this._animalAPIGateway.listAll();
    return apiResponse
      .filter((a) => this.countryHasAnimal(country, a))
      .map((a) => ({ name: a.name }));
  }

  private readonly countryHasAnimal = (
    country: Country,
    animal: AnimalResponseItem,
  ) => {
    if (country === "FR")
      return (
        animal.locations.includes("001") ||
        animal.locations.includes("EU") ||
        animal.locations.includes("FX")
      ); // etc...
    // Same logic for other countries
    return false; // We could also throw an exception but this is not the purpose of this article
  };
}

export type Animal = { name: string };
