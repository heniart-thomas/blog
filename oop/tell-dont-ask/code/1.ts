class IncreaseBankAccountBalance {
  constructor(private readonly _bankAccountRepository: BankAccountRepository) {}

  execute({ id, increase }: { id: string; increase: number }) {
    const account = this._bankAccountRepository.findById(id);
    const currentBalance = account.getBalance();
    const newBalance = currentBalance + increase;
    account.setBalance(newBalance);
    this._bankAccountRepository.save(account);
  }
}

class BankAccount {
  constructor(
    private readonly _id: number,
    private _balance: number,
  ) {}

  getBalance() {
    return this._balance;
  }

  setBalance(value: number) {
    this._balance = value;
  }
}

interface BankAccountRepository {
  findById(id: string): BankAccount;

  save(account: BankAccount): void;
}
